import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { Routes, RouterModule } from '@angular/router';
import { RoutingComponent } from './routing/routing.component';
import { ComponentsComponent } from './components/components.component';
import { ServicesComponent } from './services/services.component';
import { DirectivesComponent } from './directives/directives.component';
import { QueryParamsComponent } from './query-params/query-params.component';

import { AuthService } from './services/auth.service';
import { AuthGuard } from './services/authguard.service';
import { ComponentInteractionComponent } from './component-interaction/component-interaction.component';
import { InnerComponentInteractionComponent } from './component-interaction/inner-component-interaction/inner-component-interaction.component';
import { SiblingInnerComponent } from './component-interaction/sibling-inner/sibling-inner.component';
import { HttpClientExampleComponent } from './http-client-example/http-client-example.component';

import { HttpClientModule } from '@angular/common/http';
import { DynamicComponent } from './dynamic/dynamic.component';
import { DomManipulationComponent } from './dom-manipulation/dom-manipulation.component';
import { MyDirectiveDirective } from './directives/my-directive.directive';

const appRoutes: Routes = [
  {
    path: 'routing',
    component: RoutingComponent
  },
  {
    path: 'components',
    component: ComponentsComponent
  },
  {
    path: 'services',
    component: ServicesComponent
  },
  {
    path: 'directives/:id',
    component: DirectivesComponent
  },
  {
    path: 'qp',
    component: QueryParamsComponent
  },
  {
    path: 'allowme',
    component: ComponentsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'componentinteraction',
    component: ComponentInteractionComponent
  },
  {
    path: 'httpclient',
    component: HttpClientExampleComponent
  },
  {
    path: 'dynamiccomponents',
    component: DynamicComponent
  },
  {
    path: 'dommanipulations',
    component: DomManipulationComponent
  }
  // {
  //   path: 'changedetection',
  //   component: 
  // },
  
];

@NgModule({
  declarations: [
    AppComponent,
    RoutingComponent,
    ComponentsComponent,
    ServicesComponent,
    DirectivesComponent,
    QueryParamsComponent,
    ComponentInteractionComponent,
    InnerComponentInteractionComponent,
    SiblingInnerComponent,
    HttpClientExampleComponent,
    DynamicComponent,
    DomManipulationComponent,
    MyDirectiveDirective
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule
  ],
  providers: [AuthService, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
