import { Injectable } from '@angular/core';

@Injectable()
export class AuthService {

  isLoggedIn:boolean = false;

  constructor() { }

  login() {
    this.isLoggedIn = true;
  }

  logout() {
    this.isLoggedIn = false;
  }

  isAuthenticated() {
    const promise = new Promise(
      (resolve, reject) => {
        setTimeout(() => {
          resolve(this.isLoggedIn);
        }, 800);
      }
    );
    return promise;
  }
}
