import { Component, OnInit, ViewChild, Renderer2, ElementRef } from '@angular/core';

@Component({
  selector: 'app-dom-manipulation',
  templateUrl: './dom-manipulation.component.html',
  styleUrls: ['./dom-manipulation.component.css']
})
export class DomManipulationComponent implements OnInit {

  @ViewChild('parent') parent: ElementRef;

  tags: Array<string> = [
    'Renderer',
    'Renderer2'
  ];

  constructor(private ren: Renderer2, private el: ElementRef) { }

  ngOnInit() {
  }

  // Create Text, create element, add text to element, add element to parent 
  appendElement() {
    const p = this.ren.createElement('p');
    const text = this.ren.createText('This is paragraph appended.');
    this.ren.appendChild(p, text); // appendChild (parentNode, childNode) ACCEPTS NATIVELEMENT ONLY
    this.ren.appendChild(this.parent.nativeElement, p);
  }

  insertElementBefore() {
    const p = this.ren.createElement('p');
    const textAfter = this.ren.createText('This is after text.');
    const textBefore = this.ren.createText('This is before text.');
    console.log(this.el);
    this.ren.appendChild(p, textAfter);
    this.ren.insertBefore(p, textBefore, textAfter); // insertBefore (parentNode, childNode, afterWhichElement) ACCEPTS NATIVELEMENT ONLY
    this.ren.appendChild(this.parent.nativeElement, p);
  }

  selectSibling() {
    // this.parent.nativeElement.nextSibling()
  }

}
