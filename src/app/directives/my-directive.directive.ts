import { EventEmitter, Output, Input, Directive, ElementRef, Renderer2 } from '@angular/core';
import { HostListener, HostBinding } from '@angular/core';

@Directive({
  selector: '[appMyDirective]'
})
export class MyDirectiveDirective {

  @Input() name : string;
  // @HostBinding('class') @Input() classList: string;

  constructor(private el: ElementRef, private ren: Renderer2) { }


  // This has more preference in applying than HostBinding
  // Try to uncomment this and check, click only fires once.
  // @HostListener('mouseenter')
  // mouseOver(eventData: Event) {
  //     this.ren.setStyle(this.el.nativeElement, 'background-color', 'blue');
  // }

  // @HostListener('mouseleave')
  // mouseLeave(eventData: Event) {
  //     this.ren.setStyle(this.el.nativeElement, 'background-color', null);
  // }

  @Output() callMe = new EventEmitter<void>();

  // Bind to a style property
  @HostBinding('style.backgroundColor') backgroundColor: string;
  @HostBinding('style.color') color: string;
  @HostBinding('attr.title') title: string = 'This is dodo title!';

  @HostListener('mouseenter')
  mouseOver(eventData: Event) {
      console.log("bgcolor: ", this.backgroundColor);
      this.backgroundColor = 'blue';
      this.color = 'white';
      
      // this.ren.addClass(thiss.el.nativeElement, name);
      // console.log(this.classList);
  }


  @HostListener('mouseleave')
  mouseLeave(eventData: Event) {
    console.log("bgcolor: ", this.backgroundColor);
      this.backgroundColor = '';
      this.color = '';
      // this.ren.removeClass(this.el.nativeElement, name);
  }

  @HostListener('click')
  mouseClick(eventData: Event) {
    // console.log(this.title);
      this.backgroundColor = 'yellow';
      this.color = 'red';
      this.callMe.emit();
  }
}
