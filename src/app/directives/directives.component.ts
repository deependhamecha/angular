import { Component, OnInit } from '@angular/core';

import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-directives',
  templateUrl: './directives.component.html',
  styleUrls: ['./directives.component.css']
})
export class DirectivesComponent implements OnInit {

  dude: string = 'dude';

  constructor(private route: ActivatedRoute) { }

  id: number;

  ngOnInit() {
    // this.id = this.route.snapshot.params['id']; // Will not update

    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
    });

  }

  callMe() {
    console.log('COMING TO CALLME');
  }
}
