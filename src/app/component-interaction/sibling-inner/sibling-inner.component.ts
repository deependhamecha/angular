import { Component, OnInit } from '@angular/core';

import { CommunicatingService } from '../../services/communicating.service';

import { Subscription, Subject } from 'rxjs';

@Component({
  selector: 'app-sibling-inner',
  templateUrl: './sibling-inner.component.html',
  styleUrls: ['./sibling-inner.component.css'],
  providers: [ CommunicatingService ]
})
export class SiblingInnerComponent implements OnInit {

  name: string = 'Chava!';
  // subs: Subscription;
  
  constructor(private cs: CommunicatingService) {

  }
  
  ngOnInit() {
  }

}
