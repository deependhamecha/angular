import {  Component,
          OnInit,
          Input,
          AfterViewInit,
          OnChanges,
          SimpleChange,
          SimpleChanges,
          Output,
          EventEmitter
} from '@angular/core';

import { CommunicatingService } from '../../services/communicating.service';

@Component({
  selector: 'app-inner-component-interaction',
  templateUrl: './inner-component-interaction.component.html',
  styleUrls: ['./inner-component-interaction.component.css'],
  providers: [CommunicatingService]
})
export class InnerComponentInteractionComponent implements OnInit, AfterViewInit, OnChanges {

  @Input('name') name: string;

  private _lowerCaseName;

  @Input() count: number;

  @Input('mytrimname')
  set mytrimname(mytrimname: string) {
    this._lowerCaseName = mytrimname.toLowerCase() || 'Not Set';
    mytrimname = mytrimname.toLowerCase();
  }

  get mytrimname(): string {
    return this._lowerCaseName;
  }


  @Output('customEvent') customEvent = new EventEmitter<string>();

  constructor(private cs: CommunicatingService) {
    
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.name = 'Dude';
      // this.cs.e.emit('Deepen');
      // this.cs.sendName(this.name);
    }, 5000);

    setTimeout(() => {
      this.customEvent.emit('Sharvil Ravi Dhamecha');
    }, 10000);

    setTimeout(() => {
      this.customEvent.emit('Dilisha Ravi Dhamecha');
    }, 12000);
  }

  // ngOnChanges(changes: {[count: number] : SimpleChange}) {
  //   for(let i in changes) {
  //     console.log(changes[i]);
  //     // console.log(changes[i].currentValue);
  //   }
  // }

  ngOnChanges(changes: SimpleChanges) {
    console.log(changes);
    // console.log(changes.hasOwnProperty('name')); // Check for Variable for Change
  }
  
  childMethod1() {
    alert('This is Child Method');
  }

}
