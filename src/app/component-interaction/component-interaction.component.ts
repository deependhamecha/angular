import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';

import { InnerComponentInteractionComponent } from './inner-component-interaction/inner-component-interaction.component';

@Component({
  selector: 'app-component-interaction',
  templateUrl: './component-interaction.component.html',
  styleUrls: ['./component-interaction.component.css']
})
export class ComponentInteractionComponent implements OnInit, AfterViewInit {

  name: string = 'Deepen';
  mytrimname: string = 'Deepen Naresh Dhamecha';
  count:number = 0;

  constructor() { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.name = 'Dhamecha';
    }, 2000);

    setTimeout(() => {
      this.mytrimname = 'Dilisha Ravi Dhamecha';
    }, 7000);

    setTimeout(() => {
      console.log(this.child['_lowerCaseName']);
    }, 15000);
  }

  incr() {
    this.count++;
  }

  getMeChangedName(event: Event) {
    console.log("New Name given by child : "+event);
  }

  @ViewChild(InnerComponentInteractionComponent) child: InnerComponentInteractionComponent;

}
