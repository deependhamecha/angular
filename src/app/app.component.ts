import { Component, ViewChild, OnInit, OnChanges, SimpleChanges } from '@angular/core';

import { Router, ActivatedRoute } from '@angular/router';

import { AuthService } from './services/auth.service';

import { SiblingInnerComponent } from './component-interaction/sibling-inner/sibling-inner.component';

import { InnerComponentInteractionComponent } from './component-interaction/inner-component-interaction/inner-component-interaction.component';

import { CommunicatingService } from './services/communicating.service';

import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [CommunicatingService]
})
export class AppComponent implements OnInit {
  title = 'app';
  name: string;

  nameObs: BehaviorSubject<string>;

  constructor(private router: Router,
              private activatedRoute: ActivatedRoute,
              private authService: AuthService,
              private cs: CommunicatingService) {

                this.nameObs = new BehaviorSubject<string>('coming');
                this.nameObs.subscribe((name: string) => {
                  console.log(">>"+name);
                  this.name = name;
                });
  }

  routingVariable = 'components';

  goToServies() {
    this.router.navigate(['services']);
  }

  isLoggedIn: boolean;

  toggleLogIn() {
    this.authService.isLoggedIn = !this.authService.isLoggedIn;
    this.isLoggedIn = this.authService.isLoggedIn;
    // this.cs.sendName('Deepen');
    // this.cs.e.emit('Deepen');
  }

  @ViewChild(InnerComponentInteractionComponent) sic: InnerComponentInteractionComponent;

  ngOnInit() {
    // this.cs.obs.subscribe(
    //   name => {
    //     this.name = name;
    //   }
    // );
  }


}
