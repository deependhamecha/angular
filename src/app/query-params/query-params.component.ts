import { Component, OnInit } from '@angular/core';

import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-query-params',
  templateUrl: './query-params.component.html',
  styleUrls: ['./query-params.component.css']
})
export class QueryParamsComponent implements OnInit {

  constructor(private router: ActivatedRoute) { }
  dude = '';
  ngOnInit() {
    this.router.queryParams.subscribe((params: Params) => {
      this.dude = params['repo'];
    });
  }

}
