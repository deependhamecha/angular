import { Component, OnInit } from '@angular/core';

import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-http-client-example',
  templateUrl: './http-client-example.component.html',
  styleUrls: ['./http-client-example.component.css']
})
export class HttpClientExampleComponent implements OnInit {

  public data: any;

  constructor(private http: HttpClient) {
  }
  
  
  ngOnInit() {
    this.http.get('/assets/data.json').subscribe(
      res => {
        this.data = res;
      }, err => {
        console.log(err);
      }
    );
  }
  

}
